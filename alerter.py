#!/usr/bin/env python3

import os
import re
from flask import Flask, request, jsonify, Response

PATH = os.environ["SMS_SPOOL_DIR"]
NUMBERS = os.environ["SMS_PHONE_NUMBERS"].split(";")

STAT_FILE = os.environ.get("SMS_STAT_FILE")
SOURCE_NUMBER = os.environ.get("SMS_SOURCE_NUMBER")

app = Flask(__name__)


@app.route("/", methods=["POST"])
def send_sms():
    content = request.get_json()

    if content["state"] != "alerting":
        return jsonify(dict(status="not sending")), 200

    msg = f"{content.get('title')}\n{content.get('message')}"[:135]

    for number in NUMBERS:
        with open(f"{PATH}/{number}", "w") as f:
            f.write(f"To: {number}\n\n{msg}")

    return jsonify(dict(status="ok")), 200


@app.route("/metrics", methods=["GET"])
def metrics():
    if not STAT_FILE:
        print("STAT_FILE not defined")
        return Response("")

    with open(STAT_FILE, encoding="latin1", mode="r") as f:
        line = f.readline()
    credit = re.search("Ihr Guthaben beträgt: (\d+),(\d+)", line)
    if credit is None:
        print("regex failure")
        return Response("")
    credit = int(credit.group(1)) * 100 + int(credit.group(2))

    out = [
        "# HELP credit_remaining_cents Credit availabe to use for messages",
        "# TYPE credit_remaining_cents gauge",
        "credit_remaining_cents{phone_number="
        + str(SOURCE_NUMBER)
        + "} "
        + str(credit),
    ]
    print("\n".join(out))
    return Response("\n".join(out), mimetype="text/plain; version=0.0.4")


if __name__ == "__main__":
    print(os.environ)
    app.run(host="0.0.0.0")
