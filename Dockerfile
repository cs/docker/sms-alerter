FROM python:3.8-alpine

COPY alerter.py requirements.txt ./

RUN pip install -r requirements.txt

CMD ["./alerter.py"]
